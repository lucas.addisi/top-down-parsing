import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import fileParser.ProposedFileParser;
import model.ContextFreeGrammar;
import model.TopDownParser;

public class Main {

	public static void main(String[] args) {
		
		String test = "((()))()(())";
		
		File file = new File("./instances/grammar1");
		
		ProposedFileParser fileParser = null;
		
		try {
			fileParser = new ProposedFileParser(new FileInputStream(file));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ContextFreeGrammar grammar = fileParser.parse();
		TopDownParser parser = new TopDownParser(grammar);
	
		System.out.println(parser.accept(test));
	}

}
