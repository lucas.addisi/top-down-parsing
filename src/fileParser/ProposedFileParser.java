package fileParser;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import model.ContextFreeGrammar;
import model.Terminal;
import model.NonTerminal;
import model.ProductionBody;
import model.Symbol;

public class ProposedFileParser {

	private final Pattern nonTerminalPattern = Pattern.compile("X_\\{\\d+\\}");
	private final String replaceSymbol = "\\$";
	
	private Scanner reader;
	
	public ProposedFileParser(InputStream inputStreamFile) throws FileNotFoundException {
		
		this.reader = new Scanner(inputStreamFile);

	}
	
	public ContextFreeGrammar parse() {
		
		List<String> strProd = new ArrayList<>();
		
		Set<Terminal> terminals = new HashSet<>();
		Set<NonTerminal> nonTerminals = new HashSet<>();
		Map<NonTerminal, List<ProductionBody>> productions = new HashMap<>();
		NonTerminal initSymbol = new NonTerminal("X_{1}");
		
	    while (this.reader.hasNextLine()) 
	        strProd.add(this.reader.nextLine());    
	    
	    for(String p : strProd) {
	    	
	    	String[] splited = p.split("->");	    	   	
	    	nonTerminals.add(new NonTerminal(splited[0]));	   
	    	
	    }
	    
	    for(NonTerminal nt : nonTerminals)
	    	productions.put(nt, new ArrayList<ProductionBody>());
	    	
	    
	    for(String p : strProd) {
	    
	    	String[] splited = p.split("->");
	    	
	    	NonTerminal head = new NonTerminal(splited[0]);
	    	
	    	List<Symbol> bodySymbols = new ArrayList<>();
	    	
	    	Matcher m = this.nonTerminalPattern.matcher(splited[1]);
	    	Queue<String> nonTermQueue = new LinkedList<>();
	    	
	    	//Find Non Terminals in body string representation and put them in a queue
	    	while(m.find())
	    		nonTermQueue.add(m.group());
	    	
	    	//Change Non Terminal representation to $ symbol
	    	String replacedString = splited[1].replaceAll("X_\\{\\d+\\}", this.replaceSymbol);
	    	
	    	//for every char in str, if char is $ ? add NonTerminal : add Terminal
	    	for (char c : replacedString.toCharArray()) {
	    		
	    		if(c == '$')
	    			bodySymbols.add(new NonTerminal(nonTermQueue.poll()));
	    		else if (c == 'e')
	    			bodySymbols.add(new Terminal(""));
	    		else {
	    			
	    			Terminal ter = new Terminal(String.valueOf(c));
	    			terminals.add(ter);
	    			bodySymbols.add(ter);	    
	    			
	    		}
	    	}
	    	
	    	productions.get(head).add(new ProductionBody(bodySymbols));
	
	    }
	    		    
	    return new ContextFreeGrammar(nonTerminals, terminals, productions, initSymbol);
		
	}
	
}
