package model;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ContextFreeGrammar {

	private Set<NonTerminal> vars;
	private Set<Terminal> terminals;
	private Map<NonTerminal, List<ProductionBody>> productions;
	private NonTerminal initSymbol;

	private final Terminal emptyString = new Terminal("");
	private final Terminal stopper = new Terminal("$");

	public ContextFreeGrammar(Set<NonTerminal> vars, Set<Terminal> terminals,
			Map<NonTerminal, List<ProductionBody>> productions, NonTerminal initSymbol) {

		if (terminals.contains(this.stopper))
			throw new IllegalArgumentException("Reserved symbol $. It must not be part of terminals");

		this.vars = vars;
		this.terminals = terminals;
		this.productions = productions;
		this.initSymbol = initSymbol;

	}

	public Set<NonTerminal> getNonTerminals() {
		return vars;
	}

	public Set<Terminal> getTerminals() {
		return terminals;
	}
	
	public List<ProductionBody> getProductions(NonTerminal a){		
		return this.productions.get(a);		
	}
	
	public Terminal getEmptyString() {		
		return this.emptyString;
	}
	
	public Terminal getStopper() {
		return this.stopper;
	}

	public NonTerminal getInitSymbol() {
		return this.initSymbol;
	}
	
	/**
	 * 
	 * @param a List made of terminals and non-terminals symbols
	 * @return FIRST(a)
	 */

	public Set<Terminal> first(List<Symbol> a) {

		Set<Terminal> res = new HashSet<>();
		
		if(a.isEmpty()) {
			res.add(this.emptyString);
			return res;
		}

		for (Symbol sym : a) {

			Set<Terminal> first = this.first(sym);

			res.addAll(first);

			if (!first.contains(this.emptyString)) {
				res.remove(this.emptyString);
				break;
			}

		}

		return res;

	}

	/**
	 * 
	 * 
	 * @param s Symbol of grammar
	 * @return FIRST(s)
	 */

	public Set<Terminal> first(Symbol s) {

		Set<Terminal> toRet = new HashSet<>();

		if (this.terminals.contains(s)) {

			toRet.add((Terminal) s);
			return toRet;

		}

		if (isNulleable((NonTerminal) s))
			toRet.add(this.emptyString);

		List<ProductionBody> bodies = this.productions.get(s);

		for (ProductionBody body : bodies) {

			if (!body.isEmptyBody()) {

				Set<Terminal> first = this.first(body.get(0));

				int i = 1;
				while (i < body.size() && first.contains(this.emptyString)) {

					first.addAll(this.first(body.get(i)));
					i++;
				}

				toRet.addAll(first);

			}

		}

		return toRet;

	}

	/**
	 * 
	 * @param A NonTerminal of grammar
	 * @return Set of terminals a that can appear immediately to the right of 
	 * A in some sentential form
	 */
	public Set<Terminal> follow(NonTerminal A) {

		Set<Terminal> follow = new HashSet<>();
		
		//Add $ to FOLLOW(S), where S is the initial symbol
		if (this.initSymbol.equals(A))
			follow.add(this.stopper);
		
		for (NonTerminal term : this.productions.keySet()) {

			for (ProductionBody body : this.productions.get(term)) {

				List<Symbol> symbols = body.split();

				for (int i = 0; i < symbols.size(); i++) {
					
					if (!symbols.get(i).equals(A))
						continue;
					
					// I'm NOT standing on the last symbol of the body
					if (!(symbols.size() == i + 1)) {
						
						//Production A => \alpha B \beta with \beta nulleable
						if (this.isNulleable(symbols.get(i + 1)))
							follow.addAll(this.follow(term));
						
						//Production A => \alpha B \beta
						follow.addAll(this.first(symbols.get(i + 1)));
						
					}
					else //I'm standing on the last symbol of the body. Production A => \alpha B
						if(!symbols.get(i).equals(term))
							follow.addAll(this.follow(term));
				}

			}

		}

		follow.remove(this.emptyString);
		return follow;

	}
	
	private boolean isNulleable(Symbol symbol) {
		
		if(symbol.getClass() == Terminal.class)
			return false;
		
		List<ProductionBody> bodies = this.productions.get((NonTerminal)symbol);

		for (ProductionBody body : bodies)
			if (body.isEmptyBody())
				return true;

		return false;

	}

}
