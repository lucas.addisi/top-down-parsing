package model;

public class NonTerminal extends Symbol{
	
	
	public NonTerminal(String symbol) {
		super(symbol);
	}

	@Override
	public String toString() {
		return "NonTerminal = " + super.toString() ;
	}
	
	
	
	
}
