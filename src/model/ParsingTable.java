package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ParsingTable {

	private ContextFreeGrammar grammar;
	List<NonTerminal> nonTerms;
	List<Terminal> terms;
	Production[][] table;
	
	
	public ParsingTable(ContextFreeGrammar grammar) {
	
		this.grammar = grammar;
		this.nonTerms = new ArrayList<>(grammar.getNonTerminals()); 
		
		this.terms = new ArrayList<>(grammar.getTerminals());
		this.terms.remove(this.grammar.getEmptyString());
		this.terms.add((this.grammar.getStopper()));
		
		this.table = new Production[this.nonTerms.size()][this.terms.size()];
		
		generateParsingTable();
		
	}
	
	public Map<NonTerminal, ProductionBody> get(NonTerminal nTerm, Terminal term){
		
		int nonTermIndex = this.nonTerms.indexOf(nTerm);
		int termIndex = this.terms.indexOf(term);		
		
		Map<NonTerminal, ProductionBody> toRet = new HashMap<>();
		
		try {			
			toRet.put(this.table[nonTermIndex][termIndex].head, this.table[nonTermIndex][termIndex].body);
		}
		catch(NullPointerException e) {
			return null;
		}
				
		return toRet;
		
	}
	
	private void generateParsingTable() {
		
		List<Production> productions = new ArrayList<>();
		
		//Generate productions List
		for(NonTerminal A : this.nonTerms)
			for(ProductionBody prod : this.grammar.getProductions(A))
				productions.add(new Production(A, prod));
		
		//for each production A => \alpha
		for(Production prod : productions) {
			
			Set<Terminal> firstAlpha = this.grammar.first(prod.body.split());
			
			//for each terminal a in FIRST(\alpha)
			for(Terminal a : firstAlpha) {
				
				if (a.equals(this.grammar.getEmptyString()))
					continue;
				
				//Add A => \alpha to M[A,a]
				int nonTermIndex = this.nonTerms.indexOf(prod.head);
				int termIndex = this.terms.indexOf(a);
				
				this.table[nonTermIndex][termIndex] = prod;
				
			}
			
			
			// If \epsilon in FIRST(\alpha)
			if (firstAlpha.contains(this.grammar.getEmptyString())) {
				
				Set<Terminal> followA = this.grammar.follow(prod.head);
								
				//for each b in FOLLOW(A)
				for (Terminal b : followA) {
					
					int nonTermIndex = this.nonTerms.indexOf(prod.head);
					int termIndex = this.terms.indexOf(b);
					
					this.table[nonTermIndex][termIndex] = prod;
				}
				
				
				if(followA.contains(this.grammar.getStopper())) {
					
					int nonTermIndex = this.nonTerms.indexOf(prod.head);
					int termIndex = this.terms.indexOf(this.grammar.getStopper());
					
					this.table[nonTermIndex][termIndex] = prod;
					
				}
				
			}
		}	

	}
	
	//TODO: Esto acá está feo. Debería ser parte del la clase ContextFreeGrammar
	private class Production{
		
		public NonTerminal head;
		public ProductionBody body;
		
		public Production(NonTerminal nTerm, ProductionBody term) {
			this.head = nTerm;
			this.body = term;
		}		
	}
	
}
