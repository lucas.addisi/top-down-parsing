package model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class ProductionBody {
	
	private List<Symbol> symbols;
	private Terminal emptyString = new Terminal("");

	public ProductionBody(List<Symbol> body) {
		
		if (body.size() == 1)
			this.symbols = new ArrayList<>(body);
		else {
			
			this.symbols = new ArrayList<>();
			
			for (Symbol sym : body)
				if(!sym.equals(this.emptyString))
					this.symbols.add(sym);
			
		}		
		
	}
	
	public ProductionBody(Symbol ... symbols) {
		
		this(Arrays.asList(symbols));
		
	}
	

	public boolean isEmptyBody() {
		
		return 	this.symbols.size() == 1 && 
				this.symbols.get(0).equals(this.emptyString);
		
	}
	
	public int size() {
		
		if (this.isEmptyBody())
			return 0;
		
		return this.symbols.size();
		
	}
	
	public Symbol get(int i) {
		
		if (this.isEmptyBody())
			throw new IllegalStateException("Can't get index of empty body");
		
		return this.symbols.get(i);
		
	}
	
	public List<Symbol> split(){
		
		if (this.isEmptyBody())
			return new ArrayList<Symbol>();
		
		return this.symbols;
		
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((emptyString == null) ? 0 : emptyString.hashCode());
		result = prime * result + ((symbols == null) ? 0 : symbols.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductionBody other = (ProductionBody) obj;
		if (emptyString == null) {
			if (other.emptyString != null)
				return false;
		} else if (!emptyString.equals(other.emptyString))
			return false;
		if (symbols == null) {
			if (other.symbols != null)
				return false;
		} else if (!symbols.equals(other.symbols))
			return false;
		return true;
	}
	
	
}
