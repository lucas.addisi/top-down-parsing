package model;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Stack;

public class TopDownParser {

	private ParsingTable pTable;
	private ContextFreeGrammar grammar;
	private Stack<Symbol> stack;
	
	public TopDownParser(ContextFreeGrammar grammar) {
		
		this.grammar = grammar;
		this.pTable = new ParsingTable(grammar);
		this.stack = new Stack<>();
		
		this.stack.add(this.grammar.getStopper());
		this.stack.add(this.grammar.getInitSymbol());
		
	}
	
	public boolean accept(String string) { //FIXME: This method does not accept composed symbol. i.e: "id" (Tokenization)
		
		List<Terminal> terminalString = new ArrayList<>();
		
		for(char c : string.toCharArray())
			terminalString.add(new Terminal(String.valueOf(c)));
		
		return accept(terminalString);
	}
	
	public boolean accept(List<Terminal> string) {
		
		List<Terminal> cString = new ArrayList<>(string);
		cString.add(this.grammar.getStopper());
		
		Symbol X = this.stack.peek();
		Terminal a = cString.remove(0);
		
		while (!X.equals(this.grammar.getStopper())) {
			
			if(X.equals(a)) {
				this.stack.pop();
				a = cString.remove(0); //a is the next symbol of w
			}
			
			else if(X.getClass() == Terminal.class)
				return false; //Should throw exception
			
			else if(this.pTable.get((NonTerminal)X, a) == null)
				return false; //Should throw exception
			
			else {
				//Shouls output the derivationAcá se podría imprimir la derivación a usar
				Map<NonTerminal, ProductionBody> prod = this.pTable.get((NonTerminal) X, a);
				this.stack.pop();
				
				List<Symbol> prodSymbols = prod.get( (NonTerminal) X).split();
				ListIterator<Symbol> li = prodSymbols.listIterator(prodSymbols.size());
				
				while(li.hasPrevious())
					this.stack.push(li.previous());
				
			}
			
			X = this.stack.peek();
			
		}
		
		return true;
	}
	
}
