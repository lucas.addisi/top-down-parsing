package fileParser;

import static org.junit.jupiter.api.Assertions.*;


import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import model.ContextFreeGrammar;
import model.NonTerminal;
import model.ProductionBody;
import model.Terminal;

class ProposedFileParserTest {

	private InputStream inStream;
	
	/*
	 * Grammar:
	 * 
	 * X_{1} => X_{3} X_{2} 
	 * X_{2} => + X_{3} X_{2}
	 * X_{2} => \epsilon 
	 * X_{3} => (X{1})
	 * X_{3} => id
	 * 
	 */
	
	@BeforeEach
	void setUp() throws Exception {
		
		String p1 = "X_{1}->X_{2}X_{3}\n";
		String p2 = "X_{1}->+X_{3}X_{2}\n";
		String p3 = "X_{2}->e\n";
		String p4 = "X_{3}->(X_{1})\n";
		String p5 = "X_{3}->a\n";
		
		this.inStream = new ByteArrayInputStream((p1 + p2 + p3 + p4 + p5).getBytes());
	}

	@Test
	void test() {
		
		ProposedFileParser parser = null;
		
		NonTerminal nt1 = new NonTerminal("X_{1}");
		NonTerminal nt2 = new NonTerminal("X_{2}");
		NonTerminal nt3 = new NonTerminal("X_{3}");
		
		Terminal t1 = new Terminal("a");
		Terminal t2 = new Terminal("(");
		Terminal t3 = new Terminal(")");
		Terminal t4 = new Terminal("+");
		Terminal empty = new Terminal("");
		
		Set<NonTerminal> vars = new HashSet<>(Arrays.asList(nt1, nt2, nt3));

		Set<Terminal> terminals = new HashSet<>(Arrays.asList(t1, t2, t3, t4));

		ProductionBody prod1 = new ProductionBody(nt2, nt3);
		ProductionBody prod2 = new ProductionBody(t4, nt3, nt2);
		ProductionBody prod3 = new ProductionBody(empty);
		ProductionBody prod4 = new ProductionBody(t2, nt1, t3);
		ProductionBody prod5 = new ProductionBody(t1);

		Map<NonTerminal, List<ProductionBody>> productions = new HashMap<>();

		productions.put(nt1, Arrays.asList(prod1, prod2));
		productions.put(nt2, Arrays.asList(prod3));
		productions.put(nt3, Arrays.asList(prod4, prod5));

		ContextFreeGrammar grammar = new ContextFreeGrammar(vars, terminals, productions, nt1);
		
		ContextFreeGrammar res;
		
		try {
			parser = new ProposedFileParser(this.inStream);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		res = parser.parse();
		
		assertEquals(grammar.getInitSymbol(), res.getInitSymbol());
		assertEquals(grammar.getNonTerminals(), res.getNonTerminals());
		assertEquals(grammar.getTerminals(), res.getTerminals());
		
		for(NonTerminal nt : grammar.getNonTerminals())
			assertEquals(grammar.getProductions(nt), res.getProductions(nt));
		
	}

}
