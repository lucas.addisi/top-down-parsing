package model;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


class ContextFreeGrammarTest {

	public ContextFreeGrammar grammar;

	/*
	 * Grammar:
	 * 
	 * E => T E' 
	 * E' => + T E' | \epsilon 
	 * T => (E) | id
	 * 
	 */

	@BeforeEach
	void setUp() throws Exception {

		NonTerminal var1 = new NonTerminal("E");
		NonTerminal var2 = new NonTerminal("E'");
		NonTerminal var3 = new NonTerminal("T");

		Terminal empty = new Terminal("");
		Terminal terminal1 = new Terminal("(");
		Terminal terminal2 = new Terminal(")");
		Terminal terminal3 = new Terminal("+");
		Terminal terminal4 = new Terminal("id");

		Set<NonTerminal> vars = new HashSet<>(Arrays.asList(var1, var2, var3));

		Set<Terminal> terminals = new HashSet<>(Arrays.asList(empty, terminal1, terminal2, terminal3, terminal4));

		ProductionBody prod1 = new ProductionBody(var3, var2);
		ProductionBody prod2 = new ProductionBody(terminal3, var3, var2);
		ProductionBody prod3 = new ProductionBody(empty);
		ProductionBody prod4 = new ProductionBody(terminal1, var1, terminal2);
		ProductionBody prod5 = new ProductionBody(terminal4);

		Map<NonTerminal, List<ProductionBody>> productions = new HashMap<>();

		productions.put(var1, Arrays.asList(prod1));
		productions.put(var2, Arrays.asList(prod2, prod3));
		productions.put(var3, Arrays.asList(prod4, prod5));

		this.grammar = new ContextFreeGrammar(vars, terminals, productions, var1);

	}

	@Test
	void testFirstOfTerminal() {

		Terminal ter = new Terminal("(");

		Set<Terminal> res = new HashSet<>(Arrays.asList(new Terminal("(")));

		assertEquals(res, this.grammar.first(ter));

	}

	@Test
	void testFirstOfNonTerminal1() {

		NonTerminal nonTer = new NonTerminal("E");

		Set<Terminal> res = new HashSet<>(Arrays.asList(new Terminal("("), new Terminal("id")));

		assertEquals(res, this.grammar.first(nonTer));

	}
	
	@Test
	void testFirstOfNonTerminal2() {

		NonTerminal nonTer = new NonTerminal("E'");

		Set<Terminal> res = new HashSet<>(Arrays.asList(new Terminal("+"), new Terminal("")));

		assertEquals(res, this.grammar.first(nonTer));

	}

	@Test
	void testFirstOfNulleableSymbol() {

		NonTerminal nonTer = new NonTerminal("E'");

		Set<Terminal> res = new HashSet<>(Arrays.asList(new Terminal("+"), new Terminal("")));

		assertEquals(res, this.grammar.first(nonTer));

	}

	@Test
	void testFirstOfEmpty() {

		Terminal term = new Terminal("");

		Set<Terminal> res = new HashSet<>(Arrays.asList(new Terminal("")));

		assertEquals(res, this.grammar.first(term));

	}

	@Test
	void testFirstOfString() {

		List<Symbol> str = Arrays.asList(new Terminal("("), new NonTerminal("E"), new Terminal(")"));

		Set<Terminal> res = new HashSet<>(Arrays.asList(new Terminal("(")));

		assertEquals(res, this.grammar.first(str));

	}

	@Test
	void testFirstOfString2() {

		List<Symbol> str = Arrays.asList(new NonTerminal("E'"), new NonTerminal("T"));

		Set<Terminal> res = new HashSet<>(Arrays.asList(new Terminal("+"), new Terminal("("), new Terminal("id")));

		assertEquals(res, this.grammar.first(str));

	}

	@Test
	void testFollow1() {

		NonTerminal nonTer = new NonTerminal("E");

		Set<Terminal> res = new HashSet<>(Arrays.asList(new Terminal(")"), new Terminal("$")));

		assertEquals(res, this.grammar.follow(nonTer));

	}

	@Test
	void testFollow2() {

		NonTerminal nonTer = new NonTerminal("E'");

		Set<Terminal> res = new HashSet<>(Arrays.asList(new Terminal(")"), new Terminal("$")));

		assertEquals(res, this.grammar.follow(nonTer));

	}

	@Test
	void testFollow3() {

		NonTerminal nonTer = new NonTerminal("T");

		Set<Terminal> res = new HashSet<>(Arrays.asList(new Terminal(")"), new Terminal("+"), new Terminal("$")));

		assertEquals(res, this.grammar.follow(nonTer));

	}

}
