package model;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


class ParsingTableTest {

	private ContextFreeGrammar grammar;
	private ParsingTable parsingTable;
	
	/*
	 * Grammar:
	 * 
	 * E => T E' 
	 * E' => + T E' | \epsilon 
	 * T => (E) | id
	 * 
	 */
	@BeforeEach
	void setUp() throws Exception {
		
		NonTerminal var1 = new NonTerminal("E");
		NonTerminal var2 = new NonTerminal("E'");
		NonTerminal var3 = new NonTerminal("T");

		Terminal empty = new Terminal("");
		Terminal terminal1 = new Terminal("(");
		Terminal terminal2 = new Terminal(")");
		Terminal terminal3 = new Terminal("+");
		Terminal terminal4 = new Terminal("id");

		Set<NonTerminal> vars = new HashSet<>(Arrays.asList(var1, var2, var3));

		Set<Terminal> terminals = new HashSet<>(Arrays.asList(empty, terminal1, terminal2, terminal3, terminal4));

		ProductionBody prod1 = new ProductionBody(var3, var2);
		ProductionBody prod2 = new ProductionBody(terminal3, var3, var2);
		ProductionBody prod3 = new ProductionBody(empty);
		ProductionBody prod4 = new ProductionBody(terminal1, var1, terminal2);
		ProductionBody prod5 = new ProductionBody(terminal4);

		Map<NonTerminal, List<ProductionBody>> productions = new HashMap<>();

		productions.put(var1, Arrays.asList(prod1));
		productions.put(var2, Arrays.asList(prod2, prod3));
		productions.put(var3, Arrays.asList(prod4, prod5));

		this.grammar = new ContextFreeGrammar(vars, terminals, productions, var1);
		
		this.parsingTable = new ParsingTable(this.grammar);
		
	}

	@Test //M[E',+] = E' => + T E'
	void testParsingTable1() {
		
		Terminal term = new Terminal("+");
		NonTerminal nTerm = new NonTerminal("E'");
		
		Map<NonTerminal, ProductionBody> res = new HashMap<>();
		
		res.put(nTerm, new ProductionBody(new Terminal("+"), new NonTerminal("T"), new NonTerminal("E'")));
		
		assertEquals(res, this.parsingTable.get(nTerm, term));
		
	}
	
	@Test //M[E',$] = E' => \epsilon
	void testParsingTable2() {
		
		Terminal term = new Terminal("$");
		NonTerminal nTerm = new NonTerminal("E'");
		
		Map<NonTerminal, ProductionBody> res = new HashMap<>();
		
		res.put(nTerm, new ProductionBody(new Terminal("")));
		
		assertEquals(res, this.parsingTable.get(nTerm, term));
		
	}
	
	@Test //M[T,(] = T => ( E )
	void testParsingTable3() {
		
		Terminal term = new Terminal("(");
		NonTerminal nTerm = new NonTerminal("T");
		
		Map<NonTerminal, ProductionBody> res = new HashMap<>();
		
		res.put(nTerm, new ProductionBody(new Terminal("("), new NonTerminal("E"), new Terminal(")")));
		
		assertEquals(res, this.parsingTable.get(nTerm, term));
		
	}
	
	@Test //M[E,id] = E => T E'
	void testParsingTable4() {
		
		Terminal term = new Terminal("id");
		NonTerminal nTerm = new NonTerminal("E");
		
		Map<NonTerminal, ProductionBody> res = new HashMap<>();
		
		res.put(nTerm, new ProductionBody(new NonTerminal("T"), new NonTerminal("E'")));
		
		assertEquals(res, this.parsingTable.get(nTerm, term));
		
	}
	
	@Test // M[T,id] = T => id
	void testParsingTable5() {
		
		Terminal term = new Terminal("id");
		NonTerminal nTerm = new NonTerminal("T");
		
		Map<NonTerminal, ProductionBody> res = new HashMap<>();
		
		res.put(nTerm, new ProductionBody(new Terminal("id")));
		
		assertEquals(res, this.parsingTable.get(nTerm, term));
		
	}	
	
	@Test //M[E',)] = E' => \empty
	void testParsingTable6() {
		
		Terminal term = new Terminal(")");
		NonTerminal nTerm = new NonTerminal("E'");
		
		Map<NonTerminal, ProductionBody> res = new HashMap<>();
		
		res.put(nTerm, new ProductionBody(new Terminal("")));
		
		assertEquals(res, this.parsingTable.get(nTerm, term));
		
	}

}
