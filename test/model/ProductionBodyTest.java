package model;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


class ProductionBodyTest {

	ProductionBody body;
	ProductionBody emptyBody;
	
	@BeforeEach
	void setUp() throws Exception {
		
		Terminal empty = new Terminal("");
		NonTerminal nonT = new NonTerminal("E");
		Terminal ter1 = new Terminal("+");
		Terminal ter2 = new Terminal(")");
		
		this.body = new ProductionBody(nonT, empty, ter1, ter2);
		this.emptyBody = new ProductionBody(empty);
		
	}

	@Test
	void testIsEmptyBodyTrue() {
		
		assertTrue(this.emptyBody.isEmptyBody());
		
	}

	@Test
	void testIsEmptyBodyFalse() {
		
		assertFalse(this.body.isEmptyBody());
		
	}
	
	@Test
	void testSize() {
		
		assertTrue(this.body.size() == 3);
		
	}
	
	@Test
	void testSize2() {
		
		ProductionBody body = new ProductionBody(new Terminal("1"));
		
		assertTrue(body.size() == 1);
		
	}

	@Test
	void testSizeEmtpy() {
		
		assertTrue(this.emptyBody.size() == 0);
		
	}

	
	@Test
	void testGet() {
		
		Terminal exp = new Terminal(")");
	
		assertEquals(exp, this.body.get(2));
		
	}
	
	@Test
	void testGetEmpty() {
		
		assertThrows(IllegalStateException.class, () -> this.emptyBody.get(0));

	}		

	@Test
	void testSplit() {
		
		NonTerminal nonT = new NonTerminal("E");
		Terminal ter1 = new Terminal("+");
		Terminal ter2 = new Terminal(")");
		
		List<Symbol> exp = Arrays.asList(nonT, ter1, ter2);
		
		assertEquals(exp, this.body.split());
		
	}
	
	@Test
	void testSplitEmpty() {
		
		assertEquals(this.emptyBody.split(), new ArrayList<Symbol>());
		
	}
	

}
