package model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SymbolTest {

	@Test
	void testNotEquals1() {
		
		Symbol nonTerminal = new NonTerminal("x1");
		Symbol terminal = new Terminal("x1");
		
		assertNotEquals(nonTerminal, terminal);
		
	}
	
	@Test
	void testNotEquals2() {
		
		Symbol nonTerminal = new Terminal("x1");
		Symbol terminal = new Terminal("x2");
		
		assertNotEquals(nonTerminal, terminal);
		
	}

	@Test
	void testEquals() {
		
		Symbol nonTerminal = new Terminal("");
		Symbol terminal = new Terminal("");
		
		assertEquals(nonTerminal, terminal);
		
	}
	
	@Test
	void testEquals2() {
		
		Symbol nonTerminal = new NonTerminal("a");
		Symbol terminal = new NonTerminal("a");
		
		assertEquals(nonTerminal, terminal);
		
	}
	
	@Test
	void testEquals3() {
		
		Symbol terminal1 = new Terminal("(");
		Terminal terminal2 = new Terminal("(");
		
		assertEquals(terminal1, terminal2);
		
	}
		
}
