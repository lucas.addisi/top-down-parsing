package model;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

class TopDownParserTest {

	public TopDownParser tdParser;

	/*
	 * Grammar:
	 * 
	 * E => T E' 
	 * E' => + T E' | \epsilon 
	 * T => (E) | id
	 * 
	 */

	@BeforeEach
	void setUp() throws Exception {

		NonTerminal var1 = new NonTerminal("E");
		NonTerminal var2 = new NonTerminal("E'");
		NonTerminal var3 = new NonTerminal("T");

		Terminal empty = new Terminal("");
		Terminal terminal1 = new Terminal("(");
		Terminal terminal2 = new Terminal(")");
		Terminal terminal3 = new Terminal("+");
		Terminal terminal4 = new Terminal("id");

		Set<NonTerminal> vars = new HashSet<>(Arrays.asList(var1, var2, var3));

		Set<Terminal> terminals = new HashSet<>(Arrays.asList(empty, terminal1, terminal2, terminal3, terminal4));

		ProductionBody prod1 = new ProductionBody(var3, var2);
		ProductionBody prod2 = new ProductionBody(terminal3, var3, var2);
		ProductionBody prod3 = new ProductionBody(empty);
		ProductionBody prod4 = new ProductionBody(terminal1, var1, terminal2);
		ProductionBody prod5 = new ProductionBody(terminal4);

		Map<NonTerminal, List<ProductionBody>> productions = new HashMap<>();

		productions.put(var1, Arrays.asList(prod1));
		productions.put(var2, Arrays.asList(prod2, prod3));
		productions.put(var3, Arrays.asList(prod4, prod5));

		ContextFreeGrammar grammar = new ContextFreeGrammar(vars, terminals, productions, var1);
		
		this.tdParser = new TopDownParser(grammar);

	}
	
	
	@Test //String id + (id + id)
	void testString1() {
		
		Terminal term1 = new Terminal("id");
		Terminal term2 = new Terminal("+");
		Terminal term3 = new Terminal("(");
		Terminal term4 = new Terminal(")");
		
		List<Terminal> string = Arrays.asList(term1, term2, term3, term1, term2, term1, term4);
		
		assertTrue(this.tdParser.accept(string));

	}

	@Test //String id + (id + id
	void testString2() {
		
		Terminal term1 = new Terminal("id");
		Terminal term2 = new Terminal("+");
		Terminal term3 = new Terminal("(");
		
		List<Terminal> string = Arrays.asList(term1, term2, term3, term1, term2, term1);
		
		assertFalse(this.tdParser.accept(string));

	}

	@Test //String id 
	void testString3() {
		
		Terminal term1 = new Terminal("id");
		
		List<Terminal> string = Arrays.asList(term1);
		
		assertTrue(this.tdParser.accept(string));

	}

	@Test //String (id) + (id + id)
	void testString4() {
		
		Terminal term1 = new Terminal("id");
		Terminal term2 = new Terminal("+");
		Terminal term3 = new Terminal("(");
		Terminal term4 = new Terminal(")");
		
		List<Terminal> string = Arrays.asList(term3, term1, term4, term2, term3, term1, term2, term1, term4);
		
		assertTrue(this.tdParser.accept(string));

	}
	
	@Disabled
	@Test //Empry string
	void testEmptyString() {
		
		Terminal term1 = new Terminal("");
		List<Terminal> string = Arrays.asList(term1);
		
		assertFalse(this.tdParser.accept(string));

	}
		

}
